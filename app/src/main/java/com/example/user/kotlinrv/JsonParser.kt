package com.example.user.kotlinrv

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by user on 5/26/2017.
 */

fun parsePosts(response: String?) : ArrayList<Post> {
    val list = ArrayList<Post>()
    try {
        val object1 = JSONObject(response)
        val object2 = object1.getJSONObject("data")
        val array   = object2.getJSONArray("children")
        val size    = array.length()-1
        for (i in 0..size){
            val object3   = array.getJSONObject(i)
            val object4   = object3.getJSONObject("data")
            val title     = object4.getString("title")
            val permalink = object4.getString("permalink")
            val imgList   = getImageUrls(object4)
            val source    = imgList.get(0)
            val imageUrl  = imgList.get(1)
            val gif       = imgList.get(2)
            val url       = object4.getString("url")
            val name      = object4.getString("name")
            val author    = object4.getString("author")
            val subreddit = object4.getString("subreddit")
            val domain    = object4.getString("domain")

            list.add(Post(title, permalink, source, imageUrl, gif, url, name, author, subreddit, domain))
        }
    }catch (j: JSONException){
        Log.e("parsePosts", j.toString())
    }
    return list
}

fun parseComments(response: String?, adapter: RvAdapterComm) {
    try {
        val object1 = JSONArray(response)
        val object2 = object1.getJSONObject(1)
        val object3 = object2.getJSONObject("data")
        val array = object3.getJSONArray("children")
        val size = array.length()-2
        for (i in 0..size){
            val object4 = array.getJSONObject(i)
            val object5 = object4.getJSONObject("data")
            val comment = object5.getString("body")
            adapter.addRow(comment)
        }
    }catch (j: JSONException){
        Log.e("parseComments", j.toString())
    }
}

fun getImageUrls(object4: JSONObject): ArrayList<String>{
    val list = ArrayList<String>()
    var source   = ""
    var imageUrl = ""
    var gif      = ""
    try {
        if (object4.has("preview")){
            val object5 = object4.getJSONObject("preview")
            val array2 = object5.getJSONArray("images")
            val object6 = array2.getJSONObject(0)
            val objectSource = object6.getJSONObject("source")
            source = objectSource.getString("url")
            if (object6.has("variants")){
                val objectVariants = object6.getJSONObject("variants")
                if (objectVariants.has("mp4")){
                    val objectMp4 = objectVariants.getJSONObject("mp4")
                    val objectGifSource = objectMp4.getJSONObject("source")
                    gif = objectGifSource.getString("url")
                }
            }

            val array3 = object6.getJSONArray("resolutions")
            val s = array3.length()
            if (s < 3){
                val object7 = array3.getJSONObject(1)
                imageUrl = object7.getString("url")
            }else {
                val object7 = array3.getJSONObject(2)
                imageUrl = object7.getString("url")
            }
        }
    }catch (j: JSONException){
        Log.e("getImageUrls:", j.toString())
    }

    if (gif != ""){
        list.add("")
        list.add(imageUrl)
        list.add(gif)
    }else {
        list.add(source)
        list.add(imageUrl)
        list.add(gif)
    }
    return list
}