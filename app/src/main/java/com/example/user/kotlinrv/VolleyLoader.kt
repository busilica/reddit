package com.example.user.kotlinrv

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

/**
 * Created by user on 5/24/2017.
 */

class VolleyLoader(_a: BaseActivity) : Response.Listener<String>, Response.ErrorListener {

    val a = _a

    fun loadUrl(url: String){
        val queue = Volley.newRequestQueue(a);                              Log.d(" VLdr ==>>", "| val queue = Volley.newRequestQueue(a);           |")
        queue.add(StringRequest(Request.Method.GET, url, this, this));      Log.d(" VLdr ==>>", "| queue.add(StringRequest(Request.Method.GET, url, this, this))")
    }

    override fun onResponse(response: String?) {
                                                                                                    Log.d(" VLdr ==>>", "| a.updateRvAdapter(response);                     |")
        a.updateRvAdapter(response);
    }

    override fun onErrorResponse(e: VolleyError?) {
        Log.e("onErrorResponse", e.toString())
    }
}