package com.example.user.kotlinrv

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import java.util.*
import android.support.v7.widget.LinearLayoutManager
import android.webkit.WebView
import android.webkit.WebViewClient
import com.afollestad.easyvideoplayer.EasyVideoCallback
import com.afollestad.easyvideoplayer.EasyVideoPlayer
import java.lang.Exception


/**
 * Created by user on 5/24/2017.
 */

/* 1. Posts, Comments, Drawer */

/**********Posts**********************************************************************************/

class RvHolder(itemView: View?) : RecyclerView.ViewHolder(itemView){
    val img:  ImageView = itemView?.findViewById(R.id.img_custom_row)!!
    val txt:  TextView = itemView?.findViewById(R.id.txt_custom_row)!!
    val txt2: TextView = itemView?.findViewById(R.id.txt_second_custom_row)!!
}

class RvAdapter : RecyclerView.Adapter<RvHolder>() {

    var list: ArrayList<Post> = ArrayList()
    lateinit var listener : OnClickListener

    var c : Context? = null

    interface OnClickListener {
        fun onItemClick(post: Post, pos: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewGroup: Int): RvHolder {
        c = parent?.context
        val itemView = LayoutInflater.from(c).inflate(R.layout.custom_row, parent, false)
        return RvHolder(itemView)
    }

    override fun onBindViewHolder(holder: RvHolder?, pos: Int) {
        val post = list[pos]
        val imageUrl = post.imageUrl
        if (imageUrl == ""){
            holder?.img?.visibility = View.GONE
        }else{
            Picasso.with(c).load(post.imageUrl).into(holder?.img)
        }
        holder?.txt?.text = pos.toString() + post.title
        holder?.txt2?.text = post.author + " - " + post.subreddit + " - " + post.domain
        holder?.itemView?.setOnClickListener(View.OnClickListener {
            listener.onItemClick(post, pos)
        })
    }

    override fun getItemCount(): Int = list.size
}

/**********Comments********************************************************************************/

class RvHolderComm (itemView: View?) : RecyclerView.ViewHolder(itemView){
    val txt: TextView = itemView?.findViewById(R.id.txt_comment)!!
    val txt2: TextView = itemView?.findViewById(R.id.txt_comment_second)!!
}

class RvAdapterComm : RecyclerView.Adapter<RvHolderComm>() {

    var list: ArrayList<String> = ArrayList()
    var listener: OnClickListener? = null
    var title       = ""
    var author      = ""
    var subreddit   = ""
    var domain      = ""

    interface OnClickListener {
        fun onItemClick(post: Post)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewGroup: Int): RvHolderComm = RvHolderComm(
            LayoutInflater.from(parent?.context).inflate(R.layout.activity_comments_custom_row, parent, false))

    override fun onBindViewHolder(holder: RvHolderComm?, pos: Int) {
        if (pos == 0){
            holder?.txt?.text = title
            holder?.txt2?.text = author + " - " + subreddit + " - " + domain
        }else {
            holder?.txt?.text = list[pos]
            holder?.txt2?.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int = list.size +1

    fun addRow(comm: String){
        list.add(comm)
        notifyItemInserted(list.size -1)
    }
}

/**********Drawer**********************************************************************************/

class RvHolderDrawer (itemView: View?) : RecyclerView.ViewHolder(itemView){
    val txt: TextView = itemView?.findViewById(R.id.txt_rv_drawer)!!
}

class RvAdapterDrawer : RecyclerView.Adapter<RvHolderDrawer>() {

    var list = ArrayList<String>()
    var listener: OnDrawerClickListener? = null

    interface OnDrawerClickListener {
        fun onDrawerClicked(sub: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, p1: Int): RvHolderDrawer = RvHolderDrawer(
        LayoutInflater.from(parent?.context).inflate(R.layout.drawer_rv_custom_row, parent, false)
    )
    
    override fun onBindViewHolder(holder: RvHolderDrawer?, pos: Int) {
        val subreddit = list[pos]
        holder?.txt?.text = subreddit
        holder?.itemView?.setOnClickListener(View.OnClickListener {
            listener?.onDrawerClicked(subreddit)
        })
    }

    override fun getItemCount(): Int = list.size
}

/**********EndlessRecyclerOnScrollListener*********************************************************/

abstract class EndlessRecyclerOnScrollListener(_linearLayoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {
    var previousTotal = 0 //The total number of items in the dataset after the last load
    var loading = true //True if we are still waiting for the last set of data to load
    val visibleTreshold = 5 //The minimum amount of items to have below your current scroll position before loading more
    var firstVisibleItem: Int = 0;
    var visibleItemCount:Int = 0;
    var totalItemCount:Int = 0

    private var currentPage = 1

    val linearLayoutManager = _linearLayoutManager

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        visibleItemCount = recyclerView!!.childCount
        totalItemCount = linearLayoutManager.itemCount
        firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }
        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleTreshold) {
            //End has been reached
            //Do something
            currentPage++
            onLoadMore(currentPage)
            loading = true
        }
    }

    abstract fun onLoadMore(currentPage: Int)

    fun setCurrentPage(currentPage: Int) {
        this.currentPage = currentPage
    }
}

/**********ContentAdapter**************************************************************************/

class ContentAdapter() : EasyVideoCallback{

    fun loadImage(url: String, image: ImageView, c: Context){
        image.visibility = View.VISIBLE
        Picasso.with(c).load(url).into(image)
    }

    fun  setupWebView(url: String?, webView: WebView) {
        webView.visibility = View.VISIBLE
        webView.settings?.javaScriptEnabled = true
        webView.setWebViewClient(WebViewClient())
        webView.settings?.useWideViewPort = true
        webView.setInitialScale(50)
        webView.loadUrl(url)
    }

    fun  loadGif(gif: String?, player: EasyVideoPlayer) {
        player.visibility = View.VISIBLE
        player.setCallback(this)
        player.setSource(Uri.parse(gif))
    }

    override fun onRetry(p0: EasyVideoPlayer?, p1: Uri?) {}
    override fun onPrepared(p0: EasyVideoPlayer?) {}
    override fun onStarted(p0: EasyVideoPlayer?) {}
    override fun onCompletion(p0: EasyVideoPlayer?) {}
    override fun onSubmit(p0: EasyVideoPlayer?, p1: Uri?) {}
    override fun onBuffering(p0: Int) {}
    override fun onPreparing(p0: EasyVideoPlayer?) {}
    override fun onError(p0: EasyVideoPlayer?, p1: Exception?) {}
    override fun onPaused(p0: EasyVideoPlayer?) {}
}

