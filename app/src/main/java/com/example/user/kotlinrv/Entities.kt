package com.example.user.kotlinrv

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import com.github.fernandodev.androidproperties.lib.AssetsProperties
import com.github.fernandodev.androidproperties.lib.Property
import com.mnayef.annotations.Adapter
import com.mnayef.annotations.Click
import com.mnayef.annotations.Image
import com.mnayef.annotations.Text
import com.mnayef.annotations.enums.ImageLibraries
import com.mnayef.annotations.enums.ImageSource
import java.util.*

/**
 * Created by user on 5/24/2017.
 */

class Config(context: Context?) : AssetsProperties(context) {
    @Property("rootR")      lateinit var rootR:     String
    @Property("root")       lateinit var root:      String
    @Property("raw")        lateinit var raw:       String
    @Property("jsonCount")  lateinit var jsonCount: String
    @Property("after")      lateinit var after:     String
    @Property("&raw")       lateinit var nRaw:      String
}

class Post (_title: String, _permalink: String, _source : String, _imageUrl: String, _gif: String,
            _url: String, _name: String, _author: String, _subreddit: String, _domain: String){
    val title     = _title
    val permalink = _permalink
    val source    = _source
    val imageUrl  = _imageUrl
    val gif       = _gif
    val url       = _url
    val name      = _name
    val author    = _author
    val subreddit = _subreddit
    val domain    = _domain

    @Click(R.id.card_custom_row)
    fun cardClicked(adapter: RecyclerView.Adapter<*>, position: Int, view: View) {
        Toast.makeText(view.context, "clicked " + position, Toast.LENGTH_SHORT).show()
    }

}

class ToolbarPojo (_id: Int, _title: String, _isMain: Boolean) {
    val id      = _id
    val title   = _title
    val isMain  = _isMain
}

fun getDefaultSubs() : ArrayList<String> {
    val list = ArrayList<String>()
    list.add("AskReddit")
    list.add("funny")
    list.add("todayilearned")
    list.add("science")
    list.add("worldnews")
    list.add("pics")
    list.add("IAmA")
    list.add("gaming")
    list.add("videos")
    list.add("movies")
    list.add("Music")
    list.add("aww")
    list.add("gifs")
    list.add("explainlikeimfive")
    list.add("askscience")
    list.add("books")
    list.add("LifeProTips")
    list.add("midlyinteresting")
    list.add("space")
    list.add("Showerthoughts")
    list.add("Jokes")
    return list
}