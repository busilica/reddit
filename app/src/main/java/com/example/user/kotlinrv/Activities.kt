package com.example.user.kotlinrv

import android.app.Application
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import com.afollestad.easyvideoplayer.EasyVideoCallback
import com.afollestad.easyvideoplayer.EasyVideoPlayer
import com.squareup.picasso.Picasso
import java.lang.Exception
import java.util.*
import android.view.animation.AnimationUtils
import android.view.animation.Animation
import android.widget.Adapter
import android.widget.Toast
import kotlin.collections.ArrayList


/**
 * Created by user on 5/26/2017.
 */

/* 1. Base, 2. Main, 3. Comments, 4. Content */



/**********Base************************************************************************************/

abstract class BaseActivity : AppCompatActivity() {

    lateinit var url: Config
    lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);                                                         Log.d(" Base ==>>", "----------------------------------------------------")
        setContentView(getView());                                                                  Log.d(" Base ==>>", "| setContentView(getView());                       |")
        url = Config(this);                                                                         Log.d(" Base ==>>", "| url = Config(this);                              |")
        val url = getUrl();                                                                         Log.d(" Base ==>>", "| val url = getUrl();                              |")
        if (url != ""){
            VolleyLoader(this).loadUrl(url);                                                        Log.d(" Base ==>>", "| VolleyLoader(this).loadUrl(url);                 |")
        }
        val tPojo = getToolbarNTitle();                                                             Log.d(" Base ==>>", "| val tPojo = getToolbarNTitle();                  |")
        toolbar = findViewById(tPojo.id) as Toolbar;                                                Log.d(" Base ==>>", "| toolbar = findViewById(tPojo.id) as Toolbar;     |")
        setSupportActionBar(toolbar);                                                               Log.d(" Base ==>>", "| setSupportActionBar(toolbar);                    |")
        supportActionBar?.title = tPojo.title;                                                      Log.d(" Base ==>>", "| supportActionBar?.title = tPojo.title;           |")
                                                                                                    Log.d(" Base ==>>", "| if (!tPojo.isMain){                              |")
        if (!tPojo.isMain){
            val actionbar = supportActionBar;                                                       Log.d(" Cont ==>>", "| val actionbar = supportActionBar;                |")
            actionbar!!.setDisplayHomeAsUpEnabled(true);                                            Log.d(" Cont ==>>", "| actionbar!!.setDisplayHomeAsUpEnabled(true);     |")

        }
    }

    abstract fun getView() : Int
    abstract fun getToolbarNTitle(): ToolbarPojo
    abstract fun getUrl()  : String
    abstract fun updateRvAdapter(response: String?)
}

/**********Main************************************************************************************/

class MainActivity : BaseActivity(), RvAdapter.OnClickListener, RvAdapterDrawer.OnDrawerClickListener {

    var sub = "popular"


    var adapter: RvAdapter? = null

    override fun getView(): Int {
                                                                                                    Log.d(" Main ==>>", "| getView(): Int = R.layout.activity_main          |")
        return R.layout.activity_main;
    }

    override fun getToolbarNTitle(): ToolbarPojo {
                                                                                                    Log.d(" Main ==>>", "| getToolbarNTitle(): ToolbarPojo = (R.id.toolbar, Frontpage, true);")
        return ToolbarPojo(R.id.toolbar, "Frontpage", true)
    }

    override fun getUrl(): String {
        val url = url.rootR + sub + url.raw;
                                                                                                    Log.d(" Main ==>>", "| getUrl(): String = "+url+";")
        return url
    }

    private fun initDrawer() {
        val drawer = findViewById(R.id.drawer) as DrawerLayout
        val toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        val drawerRv = findViewById(R.id.rv_drawer) as RecyclerView
        drawerRv.layoutManager = LinearLayoutManager(this)
        val adapter = RvAdapterDrawer()
        adapter.list = getDefaultSubs()
        adapter.listener = this
        drawerRv.adapter = adapter
    }

    override fun updateRvAdapter(response: String?) {
                                                                                                    Log.d(" Main ==>>", "| if (adapter == null){                            |")
        if (adapter == null){
            val rv = findViewById(R.id.recycler) as RecyclerView;                                   Log.d(" Main ==>>", "| val rv = findViewById(R.id.recycler) as RecyclerView;")
            val manager = LinearLayoutManager(this);                                                Log.d(" Main ==>>", "| val manager = LinearLayoutManager(this);         |")
            rv.layoutManager = manager;                                                             Log.d(" Main ==>>", "| rv.layoutManager = manager;                      |")
            adapter = RvAdapter();                                                                  Log.d(" Main ==>>", "| adapter = RvAdapter();                           |")
            (adapter as RvAdapter).list = parsePosts(response);
            Log.d(" Main ==>>", "| adapter.list = parsePosts(response)              |")
            rv.adapter = adapter;                                                                   Log.d(" Main ==>>", "| rv.adapter = adapter                             |")
            val vLoader = VolleyLoader(this)
            adapter?.listener = this;
            initDrawer();
            rv.addOnScrollListener(object: EndlessRecyclerOnScrollListener(manager as LinearLayoutManager){
                override fun onLoadMore(currentPage: Int) {
                    val count = 25 * (currentPage -1)
                    val post = adapter?.list?.get(count -1)
                    val urlPage = url.rootR + sub + url.jsonCount + count.toString() + url.after + post?.name + url.nRaw
                    vLoader.loadUrl(urlPage)
                }
            })
        } else {
            val tempList = parsePosts(response);
            val count = tempList.size -1
            for (i in 0..count){
                (adapter as RvAdapter).list.add(tempList.get(i))
            }
            (adapter as RvAdapter).notifyDataSetChanged()

        }

    }

    override fun onItemClick(post: Post, pos: Int) {
        val intent = Intent(this, ContentActivity::class.java)
        intent.putExtra("url", post.url)
        intent.putExtra("gif", post.gif)
        intent.putExtra("permalink", post.permalink)
        intent.putExtra("title", post.title)
        intent.putExtra("pos", pos)
        intent.putExtra("author", post.author)
        intent.putExtra("subreddit", post.subreddit)
        intent.putExtra("domain", post.domain)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);                           Log.d(" Main ==>>", "| overridePendingTransition(R.anim.slide_left, R.anim.slide_right);")

    }

    override fun onDrawerClicked(sub: String) {
        this.sub = sub;
                                                                                                    Log.d(" Main ==>>", "| onDrawerClicked(sub: String) {                   |")
        adapter = null
        val url = url.rootR + sub + url.raw
        VolleyLoader(this).loadUrl(url);                                                            Log.d(" Main ==>>", "| VolleyLoader(this).loadUrl("+url+");")
        supportActionBar?.title = sub;                                                              Log.d(" Main ==>>", "| supportActionBar?.title = sub;                   |")
    }
}

/**********Comments********************************************************************************/

class CommentsActivity : BaseActivity() {

    var title       = ""
    var author      = ""
    var subreddit   = ""
    var domain      = ""

    override fun getToolbarNTitle(): ToolbarPojo {
                                                                                                    Log.d(" Comm ==>>", "| getToolbarNTitle(): ToolbarPojo = (R.id.toolbar_comments_activity," + intent.getStringExtra("title") + ", true)")
        return ToolbarPojo(R.id.toolbar_comments_activity, intent.getStringExtra("title"), false)
    }

    override fun getView(): Int {
                                                                                                    Log.d(" Comm ==>>", "| getView(): Int = R.layout.activity_comments;     |")
        return R.layout.activity_comments
    }

    override fun getUrl(): String {
        val url = url.root + intent.getStringExtra("permalink") + ".json";                          Log.d(" Comm ==>>", "| val url = "+url+";")
        title       = intent.getStringExtra("title")
        author      = intent.getStringExtra("author")
        subreddit   = intent.getStringExtra("subreddit")
        domain      = intent.getStringExtra("domain")
        return url
    }

    override fun updateRvAdapter(response: String?) {
        val rv = findViewById(R.id.recycler_comments) as RecyclerView;                              Log.d(" Comm ==>>", "| val rv = findViewById(R.id.recycler_comments) as RecyclerView;")
        rv.layoutManager = LinearLayoutManager(this);                                               Log.d(" Comm ==>>", "| rv.layoutManager = LinearLayoutManager(this);    |")
        val adapter = RvAdapterComm();                                                              Log.d(" Comm ==>>", "| val adapter = RvAdapterComm();                   |")
        adapter.title = title
        adapter.author = author
        adapter.subreddit = subreddit
        adapter.domain = domain
        rv.adapter = adapter;                                                                       Log.d(" Comm ==>>", "| rv.adapter = adapter;                            |")
        parseComments(response, adapter);                                                           Log.d(" Comm ==>>", "| parseComments(response);                         |")

        val actionbar = supportActionBar;                                                           Log.d(" Comm ==>>", "| val actionbar = supportActionBar;                |")
        actionbar!!.setDisplayHomeAsUpEnabled(true);                                                Log.d(" Comm ==>>", "| actionbar!!.setDisplayHomeAsUpEnabled(true);     |")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish();                                                                                   Log.d(" Comm ==>>", "| finish();                                        |")
        overridePendingTransition(R.anim.slide_up, R.anim.slide_down);                              Log.d(" Comm ==>>", "| overridePendingTransition(R.anim.slide_up, R.anim.slide_down);")
    }
}

/**********Content*********************************************************************************/

class ContentActivity : BaseActivity() {
    override fun getToolbarNTitle(): ToolbarPojo {
                                                                                                    Log.d(" Cont ==>>", "| getToolbarNTitle(): Pair = (R.id.toolbar_content_activity ,intent.getStringExtra("+title+ ", true)")
        return ToolbarPojo(R.id.toolbar_content_activity, intent.getStringExtra("title"), false)
    }

    lateinit var image:   ImageView
    lateinit var player:  EasyVideoPlayer
    lateinit var webView: WebView

    override fun getView():         Int    = R.layout.activity_content

    override fun getUrl(): String {
        val url    = intent.getStringExtra("url")
        val gif    = intent.getStringExtra("gif")

        setupViews()
        val cAdapter = ContentAdapter()

        when(gif){
            "" -> {
                if (url.contains(".jpg")){
                    cAdapter.loadImage(url, image, this)//loadImage(url)
                }else{
                    cAdapter.setupWebView(url, webView)
                }
            }
            else -> cAdapter.loadGif(gif, player)
        }


        setupCommentsBtn()
        return ""
    }

    private fun setupViews() {
        image = findViewById(R.id.img_content_activity) as ImageView
        image.visibility = View.GONE
        player  = findViewById(R.id.gif_player) as EasyVideoPlayer
        player.visibility = View.GONE
        webView = findViewById(R.id.webview) as WebView
        webView.visibility = View.GONE
    }

    private fun setupCommentsBtn() {
        val btn = findViewById(R.id.btn_content_activity);                                          Log.d(" Cont ==>>", "| val btn = findViewById(R.id.btn_content_activity);")
        val commentsIntent = Intent(this, CommentsActivity::class.java);                            Log.d(" Cont ==>>", "| val commentsIntent = Intent(this, CommentsActivity::class.java);")
        commentsIntent.putExtra("permalink", intent.getStringExtra("permalink"));                   Log.d(" Cont ==>>", "| commentsIntent.putExtra(permalink, intent.getStringExtra(permalink));")
        commentsIntent.putExtra("title", intent.getStringExtra("title"));                           Log.d(" Cont ==>>", "| commentsIntent.putExtra(title, intent.getStringExtra(title));")
        commentsIntent.putExtra("author", intent.getStringExtra("author"))
        commentsIntent.putExtra("subreddit", intent.getStringExtra("subreddit"))
        commentsIntent.putExtra("domain", intent.getStringExtra("domain"))

        btn.setOnClickListener(View.OnClickListener {                                                                                                    Log.d(" Cont ==>>", "| startActivity(commentsIntent);                   |")
            startActivity(commentsIntent);
            overridePendingTransition(R.anim.slide_up, R.anim.slide_down);                          Log.d(" Cont ==>>", "| overridePendingTransition(R.anim.slide_up, R.anim.slide_down);")
        })


    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish();                                                                                   Log.d(" Comm ==>>", "| finish();                                        |")
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);                           Log.d(" Comm ==>>", "| overridePendingTransition(R.anim.slide_left, R.anim.slide_right);")
    }

    override fun updateRvAdapter(response: String?) {}
}
